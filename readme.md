## Starter for Laravel 5.1 (LTS)

This starter project has fewer additions to "larvel new < project >"

1. Small guide for dev using homestead / production deployment
1. UserController
1. Sample User Model, Migration, Seeder
1. Rest API using Resource Controller
1. JSON protection prefix for AngularJS
1. Error messages for no data found using laravel internationalization
   
## Getting started

1. On your machine, goto Code directory (directory which is mounted on your homestead where all your projects resides) 
1. Clone this repo using `git clone url myapp`
1. Goto Homestead directory and ssh `vagrant ssh`
1. Goto your project base `cd /home/vagrant/laravelapps/myapp`
1. Install/Update Laravel framework
  1. Install packages from lock files `composer install --optimize-autoloader` / `composer install -o`
    * _Note: install option is usefull if you want to keep multiple projects on same version as this starter_
  1. To get the latest version of laravel framework and update your lock file use `composer update --optimize-autoloader` / `composer update -o`
1. Generate the new App key `artisan key:generate`
    * _Note Homestead has alias `artisan` for `php artisan` command_
1. Create MySQL DB `mysql -uhomestead -psecret -e "CREATE DATABASE IF NOT EXISTS `laravel_myapp`` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci";` 
1. Create nginx site `serve myapp.app /home/vagrant/laravelapps/myapp/public/`
1. Switch to host machine and open _Homestead.yaml_ file located at `~/.homestead` directory
  1. Add site entry
```
    - map: myapp.app
      to: /home/vagrant/laravelapps/myapp/public
```
  1. Add database entry
    `- laravel_myapp`
  
1. Creating DB user 
  1. Open _http://phpMyAdmin.app_, login using _homestead/secret_
  1. Select db _laravel_myapp_
  1. Select Privileges tab
  1. Click on Add user
  1. Give username and password, Click GO   
1. Open the code in editor of your choice
1. Edit the `.env` file to update _APP_URL_, _DB_DATABASE_, _DB_USERNAME_ and _DB_PASSWORD_
1. Open _C:\Windows\System32\drivers\etc\hosts_ file and add `192.168.10.10 myapp.app`
    * `192.168.10.10` is the IP of your homestead 
1. Hit _http://myapp.app_, You can see Laravel 5 on the page
1. Creating tables and data
  1. Switch to homestead box
  1. goto myapp directory `cd /home/vagrant/laravelapps/myapp`
  1. Create Tables using migration `artisan migrate`
  1. Seed data `artisan db:seed`
1. To test your Rest APIs hit following URLs
    * _http://myapp.app/user_
    * _http://myapp.app/user/1_

1. Add Models, Routes and Controllers. Enjoy the power of Laravel. Happy Coding
 
### License

[MIT license](http://opensource.org/licenses/MIT)