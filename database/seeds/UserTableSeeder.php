<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
          'email' => 'admin@app.in',
          'username' => 'admin',
          'password' => Hash::make('welcome'),
          'first_name' => 'Admin',
          'last_name' => 'System'
        ));
        
        User::create(array(
          'email' => 'user@app.in',
          'username' => 'user',
          'password' => Hash::make('welcome'),
          'first_name' => 'User',
          'last_name' => 'App'
        ));
    }
}
