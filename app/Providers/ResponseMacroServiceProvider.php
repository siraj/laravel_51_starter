<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;
use App\Http\Controllers\Controller;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @param  ResponseFactory  $factory
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {
        
        $factory->macro('sjson', function($contents){
            if($contents == null || $contents == ''){
                $apiErrorKey = 'api.'.Controller::getRouter()->getCurrentRoute()->getName();
                $contents = ["error" => trans($apiErrorKey)];
                $statusCode = 404;
            }
            else{
                $statusCode = 200;  
            }
            $json = json_encode($contents);
            $response = env('JSON_PROTECTION_PREFIX', '') .  $json;
        
            $response = response($response, $statusCode)
                            ->header('Content-Type', 'application/json');
        
            return $response;
        });
        
        parent::boot($factory);
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
